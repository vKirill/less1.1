Repo to docker compose project
==========================

Состоит из трех контейнеров.  
web  
nginx  
db  
  

В контейнере web выполняется приложение django https://gitlab.com/chumkaska1/django_blog.git  



Как запускать  
```
docker-compose build
docker-compose up -d
```  
  
Что происходит под "капотом"  
  
docker-compose создает выделенную сеть django_network куда будут подключены контейнеры описанные в services.  
собирает web используя ./web/Dockerfile  
после этого запускает набор контейнеров на базе собранного образа web и двух образов из docker hub: nginx и postgres  

результат успешного запуска окружения можно посмотреть следующим образом    
```
docker-compose ps
     Name                   Command               State               Ports             
----------------------------------------------------------------------------------------
less11_db_1      docker-entrypoint.sh postgres    Up      5432/tcp       
less11_nginx_1   /docker-entrypoint.sh ngin ...   Up      80/tcp, 0.0.0.0:8000->8000/tcp
less11_web_1     bash -c python manage.py m ...   Up      8001/tcp  
```
  
 
все запросы извне будут спроксированы через nginx c 8000 внешнего порта, на 8001 порт контейнера web внутри сети django_network  
напрямую web:8001 недоступен  
измения в портах сделал для наглядности перехода из 8000 внешнего в 8001 внутренний


проверка полной работоспособсности окружения производится через обращение к внешнему адресу:8000   
```
curl --silent -i http://EXT_IP:8000/ | head -n 15
HTTP/1.1 200 OK
Server: nginx/1.19.2
Date: Mon, 24 Aug 2020 13:35:12 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 2084
Connection: keep-alive
X-Frame-Options: DENY
Vary: Cookie
X-Content-Type-Options: nosniff

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
```
